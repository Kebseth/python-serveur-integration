import redis
import time
from flask import Flask, request  # pip install flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

@app.route("/")
def hello():
    # coucou
    return(str(cache.keys()))

@app.route('/', methods=['POST'])
def parse_request():
    request.get_data()
    data = request.data
    key = time.time()
    cache.set(key, data)
    return "Register"
	
if __name__ == "__main__":
    app.run(host= '0.0.0.0')
